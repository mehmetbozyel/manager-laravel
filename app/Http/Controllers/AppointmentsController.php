<?php

namespace App\Http\Controllers;

use App\appointment;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class AppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Appointment::where('user_id', auth()->user()->id)->get();
    }

    public function api_post(Request $request){
        
        $lat = $request->lat;
        $lng = $request->lng;

        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
        ])->post('http://www.mapquestapi.com/directions/v2/routematrix?key=5P9VDp342dos08FbWibaZ6BPXrcESlKh', [
            "locations" => [
                "36.625524730098746,29.116149179265758",
                "$lat,$lng",
            ],
            "options" => [
                "allToAll" => false,
                "unit" => "k"
            ]
        ]);
        return response($response->getBody()->getContents());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'customerName' => 'required|string',
            'time' => 'required|string',
            'suggestedOut' => 'required|string',
            'estimatedReturn' => 'required|string',
            'distance' => 'required|string',
        ]);

        $appointment = Appointment::create([
            'user_id' => auth()->user()->id,
            'customerName' => $request->customerName,
            'time' => $request->time,
            'suggestedOut' => $request->suggestedOut,
            'estimatedReturn' => $request->estimatedReturn,
            'distance' => $request->distance,
        ]);

        return response($appointment, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, appointment $appointment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(appointment $appointment)
    {
        if ($appointment->user_id !== auth()->user()->id) {
            return response()->json('Unauthorized', 401);
        }

        $appointment->delete();

        return response()->json('Deleted appointment', 200);
    }
    public function destroyCompleted(Request $request)
    {
        // [6,9] todo ids we are passing in and want to delete
        // [5,6,9] all of the users appoin. ids

        $appointmentsToDelete = $request->appointments;

        $userAppointmentIds = auth()->user()->Appointmets->map(function ($appointment) {
            return $appointment->id;
        });

        $valid = collect($appointmentsToDelete)->every(function ($value, $key) use ($userAppointmentIds) {
            return $userAppointmentIds->contains($value);
        });

        if (!$valid) {
            return response()->json('Unauthorized', 401);
        }

        $request->validate([
            'appointments' => 'required|array',
        ]);

        Appointment::destroy($request->appointments);

        return response()->json('Deleted', 200);
    }
}

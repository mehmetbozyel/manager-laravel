<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $http = new \GuzzleHttp\Client;

        
        /*    $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password,
                ]
            ]);
            return $response->getBody(); */


            $username = $request->username;
            $password = $request->password;
            $request->request->add([
                'username' => $username,
                'password' => $password,
                'grant_type' => 'password',
                'client_id' => 2,// config('services.passport.client_id'),
                'client_secret' => 'zN7qRmlrDVakWDVZuP9GWlpD7cuD4Z5wLhkw3Cd9', //config('services.passport.client_secret'),
                'scope' => '*'
            ]);

            $tokenRequest = $request->create(
                env('APP_URL').'/oauth/token',
                'post'
            );
            $response = Route::dispatch($tokenRequest);

            if($response->getStatusCode() == 200){
                return $response->getContent();
            }
            else if ($response->getStatusCode() == 400) {
                return response()->json('Your credentials are incorrect. Please try again', $response->getStatusCode());
                //return $response->getStatusCode();
            }
            else if ($response->getStatusCode() == 401) {
                return response()->json('Your credentials are incorrect. Please try again', $response->getStatusCode());
                //return $response->getStatusCode();
            }
            else {
                return response()->json('Something went wrong on the server.');
            }   

            // return ($response->getContent());
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Logged out successfully', 200);
    }
}
